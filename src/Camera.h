#ifndef CAMERA_H
#define CAMERA_H

class Camera {
private:
	float cameraX;
	float cameraY;
	float cameraZ;
	float cameraThetaX;
	float cameraThetaY;

	bool mF, mL, mR, mB, mU, mD;
	
public:
	Camera();
	virtual ~Camera();
	virtual void position(); // position the camera
	virtual void moveLook(int x, int y);
	virtual void movingForward(bool val);
	virtual void movingLeft(bool val);
	virtual void movingRight(bool val);
	virtual void movingBackward(bool val);
	virtual void movingUp(bool val);
	virtual void movingDown(bool val);
	virtual void move(float scale); // move the camera
};

#endif
