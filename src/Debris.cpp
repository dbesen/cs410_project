
#include <GL/glut.h>
#include <stdlib.h>
#include "Debris.h"
#include "helper.h"
#include <math.h>
#include <stdio.h>

// The constructor sets up all the parameters that a single piece of debris needs
Debris::Debris(float x, float y, float z) {
	currX = x;
	currY = y;
	currZ = z;

	radius = pow(my_rand(0.2, 1.5), 2);

	float* v = getRandomVector();
	velX = v[0];
	velY = v[1];
	velZ = v[2];

	float totalVel = my_rand(0, 30);
	velX *= totalVel;
	velY *= totalVel;
	velZ *= totalVel;

	brightness = my_rand(0.5, 1.5);

	/*
	if(velX < 0) velX = -pow(velX, 2); else velX = pow(velX, 2);
	if(velY < 0) velY = -pow(velY, 2); else velY = pow(velY, 2);
	if(velZ < 0) velZ = -pow(velZ, 2); else velZ = pow(velZ, 2);
	*/

	// One of the rotation axis should be zero -- this is how things rotate in real life
	float val = my_rand(0, 9);
	if(val < 3) rotX = 0;
	else if(val > 6) rotY = 0;
	else rotZ = 0;

	// The faster a piece of debris is moving, the more it spins

	rotSpeed = totalVel * my_rand(10, 20);
	rotAngle = my_rand(0, 360);

	v = getRandomVector();
	rotX = v[0];
	rotY = v[1];
	rotZ = v[2];

	scaleX = my_rand(1, 5);
	scaleY = my_rand(1, 5);
	scaleZ = my_rand(1, 5);

	resX = (int) my_rand(4, 8);
	resY = (int) my_rand(4, 8);
}

Debris::~Debris() {
}

// This function moves the debris along its velocity vector by an amount determined by scale
void Debris::move(float scale) {
	currX += (velX * scale);
	currY += (velY * scale);
	currZ += (velZ * scale);

	rotAngle += (rotSpeed * scale);
}

// Display this piece of debris
void Debris::display() {
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	const float colorA[4] = { 0.1, 0.1, 0.1, 1.0 };
//	const float colorD[4] = { 0.4, 0.33, 0.23, 1.0 };
	float colorD[4];
	colorD[0] = 0.4 * brightness;
	colorD[1] = 0.33 * brightness;
	colorD[2] = 0.23 * brightness;
	colorD[3] = 1.0;
	const float colorS[4] = { 0.1, 0.1, 0.1, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, colorA);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, colorD);
	glMaterialfv(GL_FRONT, GL_SPECULAR, colorS);
	glMaterialf(GL_FRONT, GL_SHININESS, 100);

	glTranslatef(currX, currY, currZ);
	glRotatef(rotAngle, rotX, rotY, rotZ);

	glScalef(scaleX, scaleY, scaleZ);

	glutSolidSphere(radius, resX, resY);

	glPopMatrix();
}
