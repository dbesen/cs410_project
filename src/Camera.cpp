
#include <GL/glut.h>
#include "Camera.h"
#include <stdio.h>

#include "math.h"
#define PI 3.1415926535897932

Camera::Camera() {
	cameraX = 0;
	cameraY = 0;
	cameraZ = 0;
	cameraThetaX = 0;
	cameraThetaY = 0;
}

// Set the camera's position (during display)
void Camera::position() {
	glRotatef(cameraThetaY, 1, 0, 0);
	glRotatef(cameraThetaX, 0, 1, 0);
//	printf("%f, %f, %f\n", cameraX, cameraZ, cameraY);
	glTranslatef(cameraX, cameraZ, cameraY);
}

// Change where the camera is looking
void Camera::moveLook(int x, int y) {
	cameraThetaX += 0.5 * x;
	cameraThetaY += 0.5 * y;
	if(cameraThetaX > 360) cameraThetaX -= 360;
	if(cameraThetaX < 0) cameraThetaX += 360;
	if(cameraThetaY > 90) cameraThetaY = 90;
	if(cameraThetaY < -90) cameraThetaY = -90;
}

// All the camera's event handlers
void Camera::movingForward(bool val) {
	mF = val;
}

void Camera::movingLeft(bool val) {
	mL = val;
}

void Camera::movingRight(bool val) {
	mR = val;
}

void Camera::movingBackward(bool val) {
	mB = val;
}

void Camera::movingUp(bool val) {
	mU = val;
}

void Camera::movingDown(bool val) {
	mD = val;
}

void Camera::move(float scale) { // move the camera
	if(mF == true) {
		cameraX += scale * (-sin(cameraThetaX*PI/180)) * ( cos(cameraThetaY*PI/180));
		cameraY += scale * ( cos(cameraThetaX*PI/180)) * ( cos(cameraThetaY*PI/180));
		cameraZ += scale * ( sin(cameraThetaY*PI/180));
	}

	if(mB == true) {
		cameraX -= scale * (-sin(cameraThetaX*PI/180)) * ( cos(cameraThetaY*PI/180));
		cameraY -= scale * ( cos(cameraThetaX*PI/180)) * ( cos(cameraThetaY*PI/180));
		cameraZ -= scale * ( sin(cameraThetaY*PI/180));
	}

	if(mL == true) {
                cameraX += scale * cos(cameraThetaX*PI/180);
                cameraY += scale * sin(cameraThetaX*PI/180);
	}
	if(mR == true) {
                cameraX -= scale * cos(cameraThetaX*PI/180);
                cameraY -= scale * sin(cameraThetaX*PI/180);
	}

	if(mU == true) cameraZ -= scale;
	if(mD == true) cameraZ += scale;
}

Camera::~Camera() {
}
