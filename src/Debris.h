#ifndef DEBRIS_H
#define DEBRIS_H

// need: 3 vars for rotation vector, rotation speed, current rotation angle

class Debris {
private:
	float currX, currY, currZ;
	float velX, velY, velZ;
	float rotSpeed, rotAngle;
	float rotX, rotY, rotZ;
	float radius;
	float scaleX, scaleY, scaleZ;
	int resX, resY;
	float brightness;
public:
	Debris(float x, float y, float z);
	virtual ~Debris();
	void move(float scale);
	void display();
};

#endif
