<?
	$fp = fopen("stardestroyer.wrl", "r");
	$ignore_non_convex = false;
	$points = array();
	$in_point = false;
	$tmp = array();
	$ignore = false;
	while(!feof($fp)) {
		$line = fgets($fp);
		$line = trim($line);
		if($line == "convex FALSE") {
			$ignore = $ignore_non_convex;
		}
		if($line == ']') {
			$in_point = false;
			if(count($tmp)) {
				if(!$ignore) {
					$points[] = $tmp;
				}
				$tmp = array();
				$ignore = false;
			}
		}
		if($in_point) {
			$line = str_replace(",", "", $line);
			$parts = explode(" ", $line);
			$tmp[] = $parts;
		}
		if($line == "point [") {
			$in_point = true;
		}
	}

	$points = dedupPoints($points);

	foreach($points as $p) {
		print "glBegin(GL_POLYGON);\n";
		printNormal($p);
		foreach($p as $v) {
			if(count($v) != 3) die("error");
			$parts = join(", ", $v);
			print "glVertex3f(" . $parts . ");\n";
		}
		print "glEnd();\n";
	}

	function dedupPoints($p) {
		$ret = array();
		$h = array();
		$count = 0;
		foreach($p as $polygon) {
			$sorted_points = array();
			foreach($polygon as $points) {
				foreach($points as $v) {
					$sorted_points[] = $v;
				}
			}
			sort($sorted_points);
			$str = join(" ", $sorted_points);
			if(isset($h[$str])) {
				$count++;
			} else {
				$h[$str] = true;
				$ret[] = $polygon;
			}
		}
		return $ret;
	}

	function printNormal($p, $two = 2) {
		if(count($p) < 3) die("Error 2");
		if(count($p) <= $two) die("Error 3");
		// Get two vectors
		$v1 = -($p[1][0] - $p[0][0]);
		$v2 = -($p[1][1] - $p[0][1]);
		$v3 = -($p[1][2] - $p[0][2]);
		$u1 = -($p[$two][0] - $p[0][0]);
		$u2 = -($p[$two][1] - $p[0][1]);
		$u3 = -($p[$two][2] - $p[0][2]);
		$num1 = $v2 * $u3 - $v3 * $u2;
		$num2 = - ($v1 * $u3 - $v3 * $u1);
		$num3 = $v1 * $u2 - $v2 * $u1;

		// Normalize them
		$len = sqrt($num1 * $num1 + $num2 * $num2 + $num3 * $num3);
		if($len == 0) { // try the next point
			$two++;
			printNormal($p, $two);
			return;
		}
		$num1 /= $len;
		$num2 /= $len;
		$num3 /= $len;
		print "glNormal3f($num1, $num2, $num3);\n";
	}
?>
