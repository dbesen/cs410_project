#include <GL/glut.h>
#include <sys/time.h>
#include <list>
#include "math.h"
#include "helper.h"
#include "saveFrame.h"

#include <stdlib.h>
#include <unistd.h>

#include "Camera.h"
#include "Debris.h"

#define PI 3.1415926535897932

using namespace std;

// Note: The Y axis is up in this world

// Our camera
Camera camera;

// Variables for keeping track of where we're looking
int lastMouseX;
int lastMouseY;
bool looking = false;

// This is where all of the planetary debris is stored
list<Debris*> debrisList;

// Booleans for the state of the animation
bool planetExploded = false;
bool savingFrames = false;

// Rendering parameters
float target_frame_rate = 60; // in frames per second

const int windowX = 700;
const int windowY = 600;


// Scene positioning parameters
float planetPos[3] = {-100, 0, -200};
float deathStarPos[3] = {150, 0, -170};
float starDestroyerPos[3] = {0, 50, 0};
int numDebris = 800;
float planetRadius = 30;
float explosionLight = 1.0; // 0-1

// Function to initialize the directed scene lighting.  The explosion light is initialized in displayExplosionLight().
void makeLights() {
	GLfloat const ambient[4] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat const diffuse[4] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat const specular[4] = { 1.0, 1.0, 1.0, 1.0 };
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
//	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
	glEnable(GL_LIGHT0);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
}

// Function to update the positions of all of the debris
void moveDebris(float amt) {
	for(list<Debris*>::iterator currItem = debrisList.begin(); currItem != debrisList.end(); currItem++) {
		(*currItem)->move(amt);
	}
}

// Function to initialize all the debris
void initDebris() {
	srand(2);
	for(int i=0;i<numDebris;i++) {
		debrisList.push_front(new Debris(planetPos[0], planetPos[1], planetPos[2]));
	}

	// Move the debris out a bit because it looks silly all coming from the same point
	moveDebris(1);
}

// Function to display all of the debris
void displayDebris() {
	for(list<Debris*>::iterator currItem = debrisList.begin(); currItem != debrisList.end(); currItem++) {
		(*currItem)->display();
	}
}

// OpenGL initalization routine (called from main)
void myinit() {
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 2000.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);
	makeLights();
	initDebris();
}

// This function renders both the planet itself and the actual explosion.
void displayPlanet() {
	if(planetRadius > 200) return;
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	float colorA[4] = { 0.1, 0.1, 0.1, 1.0 };
	float colorD[4] = { 0.0, 0.0, 0.5, 1.0 };
	float colorS[4] = { 0.1, 0.1, 0.1, 1.0 };
	if(planetExploded) {
		colorD[0] = 0.0;
		colorD[1] = 0.0;
		colorD[2] = 0.0;
		colorD[3] = 1.0;
		colorA[0] = explosionLight;
		colorA[1] = explosionLight / 2.0;
		colorA[2] = 0.0;
		colorA[3] = 1.0;
		colorS[0] = 0.0;
		colorS[1] = 0.0;
		colorS[2] = 0.0;
		colorS[3] = 1.0;
	}
	glMaterialfv(GL_FRONT, GL_AMBIENT, colorA);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, colorD);
	glMaterialfv(GL_FRONT, GL_SPECULAR, colorS);
	glMaterialf(GL_FRONT, GL_SHININESS, 100);

	glTranslatef(planetPos[0], planetPos[1], planetPos[2]);
	float r = planetRadius;
	if(planetExploded) {
//		r = sqrt(r * 30) / 2;
		r = 20 * sin(r / 80) + 10;
	}
	glutSolidSphere(r, 50, 50);
	glPopMatrix();
}

// Function to initalize and display the light from the explosion (at the proper time)
void displayExplosionLight() {
	if(explosionLight <= 0) return;

	float ambient[4];
	float diffuse[4];
	float specular[4];
	ambient[0] = explosionLight;
	ambient[1] = explosionLight / 2.0;
	ambient[2] = 0.0;
	ambient[3] = 1.0;
	diffuse[0] = explosionLight;
	diffuse[1] = explosionLight / 2.0;
	diffuse[2] = 0.0;
	diffuse[3] = 1.0;
	specular[0] = explosionLight;
	specular[1] = explosionLight / 2.0;
	specular[2] = 0.0;
	specular[3] = 1.0;

	glEnable(GL_LIGHT1);
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.5);
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0);
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0);
	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specular);
	float l2pos[4];
	l2pos[0] = planetPos[0];
	l2pos[1] = planetPos[1];
	l2pos[2] = planetPos[2];
	l2pos[3] = 1.0;
	glLightfv(GL_LIGHT1, GL_POSITION, l2pos);
}

// Function to render the line which represents the death star's main weapon
void displayDeathBeam() {
	glDisable(GL_LIGHTING);
	glColor3f(0.0, 1.0, 0.0);
	glLineWidth(3.0);
	// death star line has to go out & left
	glBegin(GL_LINES);
		glVertex3f(deathStarPos[0] - 20, deathStarPos[1], deathStarPos[2] - 17);
		glVertex3f(planetPos[0], planetPos[1], planetPos[2]);
	glEnd();
	glLineWidth(1.0);
	glEnable(GL_LIGHTING);
}

// Function to display the death star
void displayDeathStar() {
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	float colorA[4] = { 0.1, 0.1, 0.1, 1.0 };
	float colorD[4] = { 0.5, 0.5, 0.5, 1.0 };
	float colorS[4] = { 0.4, 0.4, 0.4, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, colorA);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, colorD);
	glMaterialfv(GL_FRONT, GL_SPECULAR, colorS);
	glMaterialf(GL_FRONT, GL_SHININESS, 100);
	glTranslatef(deathStarPos[0], deathStarPos[1], deathStarPos[2]);
	glRotatef(-90, 1, 0, 0);
	glScalef(.01, .01, .01);
	#include "deathstar.data"
	glPopMatrix();
}

// Function to display the star destroyer
void displayStarDestroyer() {
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	float colorA[4] = { 0.1, 0.1, 0.1, 1.0 };
	float colorD[4] = { 0.5, 0.5, 0.5, 1.0 };
	float colorS[4] = { 0.5, 0.5, 0.5, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, colorA);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, colorD);
	glMaterialfv(GL_FRONT, GL_SPECULAR, colorS);
	glMaterialf(GL_FRONT, GL_SHININESS, 100);
	glTranslatef(starDestroyerPos[0], starDestroyerPos[1], starDestroyerPos[2]);
	glRotatef(-90, 1, 0, 0);
	glRotatef(90, 0, 0, 1);
	glScalef(.0002, .0002, .0002);
	#include "stardestroyer.data"
	glPopMatrix();
}

// Function to display randomized three-dimensional stars
void displayRandomStars() {
	srand(1); // same every time
	glDisable(GL_LIGHTING);
	glColor3f(1.0, 1.0, 1.0);
	float x, y, z;
	glBegin(GL_POINTS);
		for(int i=0;i<1000;i++) {
			x = my_rand(-1000, 1000);
			y = my_rand(-1000, 1000);
			z = my_rand(-1000, 1000);

			// Don't draw stars near the scene -- they're distracting
			if(fabs(x) < 500 && fabs(y) < 500 && fabs(z) < 500) continue;
			glVertex3f(x, y, z);
		}
	glEnd();
	glEnable(GL_LIGHTING);
}

// The main display function
void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat const l1pos[4] = {-1, 1, 1, 0};

	// Position the camera
	camera.position();

	glLightfv(GL_LIGHT0, GL_POSITION, l1pos);

	displayPlanet();
	if(planetExploded) {
		displayExplosionLight();
		displayDebris();
	}
	// we can use either planet radius or explosion color to time events
	if(planetExploded && explosionLight > 0.4)
		displayDeathBeam();

	displayDeathStar();
	displayStarDestroyer();

	displayRandomStars();

	glutSwapBuffers();
}


// The handler for mouse clicks.  All this is used for is toggling whether or not mouse movements orient the camera.
void onMouse(int button, int state, int x, int y) {
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		looking = !looking;
		if(looking) {
			lastMouseX = x;
			lastMouseY = y;
		}
	}
}

// The handler for mouse movement -- orient the camera
void onMousePassive(int x, int y) {
	if(!looking) return;

	camera.moveLook(x - lastMouseX, y - lastMouseY);

	lastMouseX = x;
	lastMouseY = y;
}

// Event handler for beginning the planet explosion
void explodePlanet() {
	if(planetExploded) return;
	planetExploded = true;
	planetRadius = 10;
}

// Event handler to start frame recording
void startRecording() {
	target_frame_rate = 30;
	savingFrames = true;
}

// The handler for keypress -- move the camera
void onKeyDown(unsigned char key, int x, int y) {
	if(key == 'e') camera.movingForward(true);
	if(key == 'd') camera.movingBackward(true);
	if(key == 's') camera.movingLeft(true);
	if(key == 'f') camera.movingRight(true);
	if(key == 'a') camera.movingUp(true);
	if(key == 'z') camera.movingDown(true);
	if(key == ' ') explodePlanet();
	if(key == 'r') startRecording();
}

void onKeyUp(unsigned char key, int x, int y) {
	if(key == 'e') camera.movingForward(false);
	if(key == 'd') camera.movingBackward(false);
	if(key == 's') camera.movingLeft(false);
	if(key == 'f') camera.movingRight(false);
	if(key == 'a') camera.movingUp(false);
	if(key == 'z') camera.movingDown(false);
}

// Function to return microsecond-resolution time as a double (in seconds)
double getTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (double)tv.tv_sec + (((double)tv.tv_usec) / 1000000.0);
}

// OpenGL handler to orchistrate the animation -- this handles setting the frame rate, and events
void onTimer(int value) {
	glutTimerFunc((int) (1000.0 / target_frame_rate), onTimer, 0);

	static double currTime = 0;
	if(currTime < 1) currTime = getTime();
	double lastTime = currTime;
	currTime = getTime();
	float elapsed = currTime - lastTime;

	camera.move(40 * elapsed);

	if(planetExploded) {
		moveDebris(elapsed);
		planetRadius += 100 * elapsed;
		explosionLight -= (elapsed / 3.0);
	}

	glutPostRedisplay();
	if(savingFrames) {
		saveFrame(windowX, windowY);
	}
}

// The main method -- perform all necessary setup and initialize the glut main loop
int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowX, windowY);
	glutCreateWindow("Death Star");
	glutDisplayFunc(display);
	glutMouseFunc(onMouse);
	glutPassiveMotionFunc(onMousePassive);
	glutKeyboardFunc(onKeyDown);
	glutKeyboardUpFunc(onKeyUp);
	glutTimerFunc(30, onTimer, 0);
	myinit();
	glutMainLoop();
}
