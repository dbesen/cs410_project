
#include <math.h>
#include <stdlib.h>
#include "helper.h"

// Return a random vector
float* getRandomVector() {
	// Surprisingly, all three axis are uniform -- this only occurs in 3d
	float* ret = new float[3];
	ret[0] = my_rand(-1, 1);
	ret[1] = my_rand(-1, 1);
	ret[2] = my_rand(-1, 1);
	float size = sqrt(ret[0] * ret[0] + ret[1] * ret[1] + ret[2] * ret[2]);
	ret[0] /= size;
	ret[1] /= size;
	ret[2] /= size;
	return ret;
}

// Return a random number between low and high
float my_rand(float low, float high) {
	float val = (float) rand() / (float) RAND_MAX;
	val *= (high - low);
	val += low;
	return val;
}

