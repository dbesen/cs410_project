/*
  Functions for writing the openGL frame buffer to a file. 

  John Stevens			November 29, 2007 

  Calls to the saveFrame function will store the current frame in a buffer until __MAX_FRAMES
  have been stored.  The frames will then be written to binary ppm files.  Subsequent calls to 
  saveFrame will have no effect.  Change the value of __MAX_FRAMES to change the number of frames
  written.  

  When the frames are written to disk, there will be a noticable pause.  

  The file names of the frames are automatically set.  The first frame will be written to 
  "0000000.ppm", the second frame to "0000001.ppm", etc.  
*/

#ifndef SAVEFRAME_H
#define SAVEFRAME_H


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <iostream>
#include <fstream>
#include <vector>

#define BYTES_PER_PIXEL 3

std::vector<unsigned char*> __savedFrames;

/* Number of frames to save.  Change this value to change the number of frames saved. */
const double __MAX_FRAMES = 180;

/* Writes all saved frames to disk. */
void writeFrames( int windowWidth, int windowHeight ) {
  for( unsigned frameNum = 0; frameNum < __savedFrames.size(); ++frameNum ) {
    unsigned char* buf = __savedFrames[frameNum];
     
    // file name
    std::string str;
    char num[80];
     		
    sprintf(num, "%d", frameNum);
     		
    if (frameNum < 10) str = "0" + str;
    if (frameNum < 100) str = "0" + str;
    if (frameNum < 1000) str = "0" + str;
    if (frameNum < 10000) str = "0" + str;
    if (frameNum < 100000) str = "0" + str;
    if (frameNum < 1000000) str = "0" + str;
     		
    str = str + num;
    str = str + ".ppm";
     
    // write a ppm ascii file
    std::ofstream OUTFILE(str.c_str());
     
    OUTFILE << "P6" << std::endl;
    OUTFILE << windowWidth << " " << windowHeight << std::endl;
    OUTFILE << "255" << std::endl;
     
    for( int i = 0; i < windowHeight; ++i ) {
      for( int j = 0; j < windowWidth; ++j ) {
        OUTFILE << buf[ (windowHeight-i-1)*(((windowWidth*BYTES_PER_PIXEL+3) >> 2) << 2) + BYTES_PER_PIXEL*j ] 
     		<< buf[ (windowHeight-i-1)*(((windowWidth*BYTES_PER_PIXEL+3) >> 2) << 2) + BYTES_PER_PIXEL*j + 1 ]
     		<< buf[ (windowHeight-i-1)*(((windowWidth*BYTES_PER_PIXEL+3) >> 2) << 2) + BYTES_PER_PIXEL*j + 2 ];
      }
    }
    OUTFILE.close();
    delete[] buf;
  }
  
  __savedFrames.clear();
}


/* Saves the current openGL display buffer.
   Parameters:
   windowWidth: the width of the current window
   windowHeight: the height of the current window
*/
void saveFrame( int windowWidth, int windowHeight ) {
  static int frame = 1;
  if( frame < 0 ) return;

  // get image data
  int size = (((windowWidth*BYTES_PER_PIXEL+3) >> 2) << 2)*windowHeight;
  unsigned char* newbuf = new unsigned char[size];

  glReadPixels(0, 0, windowWidth, windowHeight, GL_RGB, GL_UNSIGNED_BYTE, newbuf);

  __savedFrames.push_back(newbuf);
  
  ++frame;

  if( frame > __MAX_FRAMES ) {
    frame = -1;
    writeFrames(windowWidth, windowHeight);
  }

}


#endif
